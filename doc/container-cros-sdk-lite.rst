.. SPDX-License-Identifier: CC-BY-SA-4.0

Creating a lightweight Chrome OS SDK container
==============================================

The main method to build libcamera for Chrome OS is to use the full-blown
`Chrome OS SDK`_. The SDK provides a standalone environment to build anything
from invidivual packages to a complete Chrome OS target image. Building a full
SDK takes hours on a powerful build machine, and consumes hundreds of GBs of
disk space, making it unsuitable for usage on the freedesktop.org shared GitLab
runners.

.. _Chrome OS SDK: https://www.chromium.org/chromium-os/build/sdk-creation/

The Chrome OS SDK supports creating standalone toolchain and sysroot tarballs.
While those do no contain all the tools required to build Chrome OS packages,
it is enough to build libcamera manually. With a few additional hacks a
libcamera Portage package compatible with Chrome OS can be created manually.

This provides a solution light enough to build libcamera for Chrome OS on the
freedesktop.org shared runners. Building the container image, however, still
requires building a full Chrome OS SDK. This needs to be done outside of the CI
infrastructure, and the image must be pushed manually to the freedesktop.org
container registry.

Building the container image
----------------------------

The Chrome OS SDK can't be built inside an unprivileged container, and neither
Docker nor Podman support running their ``build`` command with privileges. To
work around this issue, the cros-sdk-lite image is build with a shell script
located in `containers/cros-sdk-lite/build.sh <../../containers/cros-sdk-lite/build.sh>`_.

Pushing the image the to freedesktop.org registry
-------------------------------------------------

In the GitLab project's
`Setting -> Repository <https://gitlab.freedesktop.org/${GITLAB_PROJECT}/-/settings/repository#js-deploy-tokens>`_,
expand the *Deploy tokens* section and click on *Add token*. On the form that
opens, select:

- *Name*: cros-sdk-lite-builder
- *Expiration date*: Next day
- *Username*: cros-sdk-lite-builder
- *Scopes*: Check *read_registry* and *write_registry*

Click on *Create deploy token*, and make a note of the token value. The
remaining of this document will refer to the token value as
``${DEPLOY_TOKEN}``.

.. code:: bash

   ~ $ podman login -u cros-sdk-lite-builder -p ${DEPLOY_TOKEN} registry.freedesktop.org
   Login Succeeded!
   ~ $ podman push localhost/cros-sdk-lite-r120-15662:2024-01-16 docker://registry.freedesktop.org/${GITLAB_PROJECT}/cros-sdk-lite-r120-15662:2024-01-16.0
   Getting image source signatures
   Copying blob 54dc9ff5151f done
   Copying blob 30b0f9676fbc done
   Copying blob 7292cf786aa8 done
   Copying blob 5e553df650b2 done
   Copying blob b54753358b75 done
   Copying config 428d6dc34e done
   Writing manifest to image destination
   Storing signature
   ~ $ podman logout registry.freedesktop.org
   Removed login credentials for registry.freedesktop.org
