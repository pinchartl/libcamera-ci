#!/bin/bash

# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: © 2023 Laurent Pinchart <laurent.pinchart@ideasonboard.com>
#
# Build libcamera

set -e

source "$(dirname "$0")/lib.sh"
source "$(dirname "$0")/build-libcamera-common.sh"

libcamera_build() {
	echo "Building libcamera history for $CI_COMMIT_REF_NAME ($base..$CI_COMMIT_SHA)"

	# Cleanup any rebase failure upon exit, to avoid affecting subsequent
	# job runs.
	trap "rm -fr ${PWD}/.git/rebase-merge" EXIT

	git rebase \
		-x "git show --no-patch HEAD" \
		-x "meson compile -C build -j$BUILD_CONCURRENT_JOBS" \
		$base
}

base=$(find_base origin/$CI_DEFAULT_BRANCH $CI_COMMIT_SHA)

if [[ $base == $CI_COMMIT_SHA ]] ; then
	echo "No commit to test, skipping"
	exit 0
fi

run libcamera_install_pkgs
run libcamera_setup
run libcamera_build collapsed
