#!/bin/bash

# SPDX-License-Identifier: GPL-2.0-only
# SPDX-FileCopyrightText: © 2023 Jacopo Mondi <jacopo.mondi@ideasonboard.com>
# SPDX-FileCopyrightText: © 2023 Laurent Pinchart <laurent.pinchart@ideasonboard.com>
#
# Build libcamera for Chrome OS

set -e

source "$(dirname "$0")/lib.sh"

libcamera_cros_setup() {
	echo "Setting up libcamera build for Chrome OS"

	meson setup \
		--auto-features disabled \
		--cross-file /opt/cros-sdk/meson.x86_64-cros-linux-gnu.amd64.ini \
		--libdir lib64 \
		--prefix /usr \
		--sysconfdir /etc/camera \
		--wrap-mode nodownload \
		-Dandroid=enabled \
		-Dandroid_platform=cros \
		-Dcam=enabled \
		-Dpipelines=ipu3,uvcvideo \
		-Dtest=false \
		-Dudev=enabled \
		-Dv4l2=false \
		build
}

libcamera_cros_build() {
	echo "Building libcamera for Chrome OS"

	meson compile -C build -j$BUILD_CONCURRENT_JOBS
}

libcamera_cros_package() {
	echo "Packaging libcamera for Chrome OS"

	meson install -C build --no-rebuild \
		--destdir install \
		--tags bin,bin-devel,runtime

	# The HAL symlink isn't created by meson.
	mkdir -p build/install/usr/lib64/camera_hal/
	ln -s ../libcamera-hal.so build/install/usr/lib64/camera_hal/libcamera-hal.so

	# The tarball name needs to end with .bz2 regardless of the compression
	# algorithm it uses.
	tar -c --zstd -f libcamera-upstream-9999.tar.bz2 -C build/install/ .

	# Now comes the hack. Unpack the XPAK that came from the packahe built
	# by Chrome OS, update the information, and repack it.
	mkdir build-info
	pushd build-info
	qxpak -x /opt/cros-sdk/libcamera-upstream-9999.xpak

	# BUILD_TIME
	date +%s > BUILD_TIME

	# No need to update CFLAGS, CXXFLAGS and LDFLAGS as we use the
	# cross-file from the SDK, the flags should be in sync.

	# NEEDED, NEEDED.ELF.2
	#
	# The code below is borrowed from bin/misc-functions.sh from Gentoo's
	# Portage.
	rm NEEDED NEEDED.ELF.2

	declare -rA multilibs=(
		['386']='x86_32',
		['AARCH64']='arm_64',
		['ARM']='arm_32',
		['X86_64']='x86_64'
	)

	declare -A provides_map
	declare -A requires_map

	local scanelf_output=$(scanelf -yRBF '%a;%p;%S;%r;%n' ../build/install/)

	if [[ -n ${scanelf_output} ]]; then
		while IFS= read -r l; do
			local arch=${l%%;*}; l=${l#*;} ; arch=${arch#EM_}
			local obj="/${l%%;*}"; l=${l#*;}
			local soname=${l%%;*}; l=${l#*;}
			local rpath=${l%%;*}; l=${l#*;}; [[ "${rpath}" = "  -  " ]] && rpath=""
			local needed=${l%%;*}; l=${l#*;}
			local multilib=${multilibs[$arch]}

			# Infer implicit soname from basename (bug 715162).
			if [[ -z ${soname} && $(file -S "${D%/}${obj}") == *"SB shared object"* ]]; then
				soname=${obj##*/}
			fi

			echo "${obj} ${needed}" >> NEEDED
			echo "${arch};${obj};${soname};${rpath};${needed};${multilib}" >> NEEDED.ELF.2

			provides_map[$multilib]="${provides_map[$multilib]} ${soname}"
			requires_map[$multilib]="${requires_map[$multilib]} ${needed//,/ }"
		done <<< ${scanelf_output}
	fi

	# PROVIDES, REQUIRES
	rm PROVIDES REQUIRES

	for multilib in ${!provides_map[@]} ; do
		# The provides_map and requires_map contain duplicate sonames
		# that need to be removed. Use associative arrays using sonames
		# as keys to do so.
		declare -A provides
		for soname in ${provides_map[$multilib]} ; do
			provides[$soname]=1
		done

		if [[ ${#provides[@]} != 0 ]] ; then
			echo "${multilib}: ${!provides[@]}" >> PROVIDES
		fi

		# REQUIRES entries need to be further filtered to remove
		# internal dependencies between libraries in the package.
		# Ignore any entry in the provides array.
		declare -A requires
		for soname in ${requires_map[$multilib]} ; do
			if [[ -z ${provides[$soname]} ]] ; then
				requires[$soname]=1
			fi
		done

		if [[ ${#requires[@]} != 0 ]] ; then
			echo "${multilib}: ${!requires[@]}" >> REQUIRES
		fi
	done

	# SIZE
	du -b ../build/install/ > SIZE

	qxpak -c ../libcamera-upstream-9999.xpak .
	popd

	# Create the final .tbz2 package.
	qtbz2 -j libcamera-upstream-9999.tar.bz2 \
		libcamera-upstream-9999.xpak \
		libcamera-upstream-9999.tbz2
}

run libcamera_cros_setup
run libcamera_cros_build collapsed
run libcamera_cros_package
