#!/bin/bash

# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: © 2024 Google Inc.

set -e

source "$(dirname "$0")/lib.sh"

libcamera_unpackage() {
	echo "Unpackage libcamera binaries"

	tar -xvf libcamera-${CI_COMMIT_SHA}.tar.xz -C /
	ldconfig -v
}

run libcamera_unpackage
