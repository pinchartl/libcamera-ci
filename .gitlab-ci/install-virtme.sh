#!/bin/bash

# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: © 2023 Laurent Pinchart <laurent.pinchart@ideasonboard.com>
#
# Build and install virtme-ng

set -e

source "$(dirname "$0")/lib.sh"

virtme_install() {
	echo "Installing virtme-ng"

	git clone \
		--branch $VIRTME_GIT_REF \
		--depth 1 \
		--recurse-submodules \
		--single-branch \
		$VIRTME_GIT_URL

	pushd virtme-ng

	# For unknown reasons, specifying a /usr prefix ends up installing in
	# /usr/local. This is fine for now, as /usr/local/bin is in the path,
	# and the corresponding lib directory in the Python path.

	BUILD_VIRTME_NG_INIT=1 \
	pip3 install \
		--prefix=/usr \
		--force-reinstall \
		--no-deps .

	popd
}

run virtme_install
