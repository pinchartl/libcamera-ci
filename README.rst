.. SPDX-License-Identifier: CC-BY-SA-4.0

==============
 libcamera CI
==============

The libcamera CI infrastructure runs on the freedesktop.org GitLab instance.

libcamera has an `official git tree on freedesktop.org`_, with a separate tree
to store the `CI implementation`_.

.. _official git tree on freedesktop.org: https://gitlab.freedesktop.org/camera/libcamera
.. _CI implementation: https://gitlab.freedesktop.org/camera/libcamera-ci


Introduction
============

The CI implementation currently supports branch pipelines only, as libcamera
doesn't use merge requests. A new pipeline is triggered for every push to a
branch, and runs the following stages:

- The *container* stage creates container images used by subsequent stages.
  Some of the container images require a too resource-intensive build process
  for the freedesktop.org shared runners and need to be `built manually
  <doc/container-cros-sdk-lite.rst>`_.

- The *build* stage builds libcamera for various platforms and with different
  compilers. Some of the builds are meant for build-testing only, while others
  package the resulting binaries in artifacts for runtime testing.

- The *lint* stage checks the source code for common issues.

- The *test* stage runs tests in virtual machines and real devices. The latter
  submit test jobs to LAVA.


Enabling CI in your libcamera clone
===================================

If you clone the libcamera repository on a GitLab instance where you have
access to runners, you can enable CI for your project in the web UI's *Settings
-> CI/CD* section. Expand the *General pipelines* section, and set the *CI/CD
configuration file* to ``gitlab-ci.yml@camera/libcamera-ci``.

All the CI jobs that run on shared runners with GitLab-build container images
will be available. This excludes the ``build-package:cros`` and ``test-soraka``
jobs.
