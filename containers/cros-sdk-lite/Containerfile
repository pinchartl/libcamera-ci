# SPDX-License-Identifier: GPL-3.0-or-later
#
# SPDX-FileCopyrightText: © 2023 Ideas On Board Oy
# Jacopo Mondi <jacopo.mondi@ideasonboard.com>
# Laurent Pinchart <laurent.pinchart@ideasonboard.com>

ARG num_jobs=$(nproc)

# Podman 3.4.4 creates directories as root when using WORKDIR, not as the
# current USER. As a workaround, some WORKDIR invocations are preceded by a
# manual mkdir.

# ------------------------------------------------------------------------------
FROM docker.io/library/debian:bookworm-slim AS cros-sdk-sources
ENV DEBIAN_FRONTEND=noninteractive

ARG cros_board=soraka-libcamera
ARG cros_sdk_revision=release-R120-15662.B

RUN apt-get update && apt-get install --no-install-recommends -y \
        ca-certificates \
        curl \
        git-core \
        python3 \
        procps \
        sudo \
        ssh \
        xz-utils

# Setup user, the CrOS SDK refuses to build as root
RUN useradd cros --create-home --shell /bin/bash
RUN echo 'cros ALL=(ALL:ALL) NOPASSWD: ALL' >> /etc/sudoers

USER cros
WORKDIR /home/cros/

# Depot tools
RUN git config --global user.email "dev@null" && \
        git config --global user.name "dev null" && \
        git config --global color.ui false
RUN git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
ENV PATH=/home/cros/depot_tools:$PATH

# CrOS SDK sources
RUN mkdir chromiumos
WORKDIR chromiumos
RUN repo init -u https://chromium.googlesource.com/chromiumos/manifest -b ${cros_sdk_revision} --depth=1 && \
        repo sync -j4

# Apply local patches
COPY patches/ ../patches/
WORKDIR src/third_party/chromiumos-overlay/
RUN git am /home/cros/patches/third_party/chromiumos-overlay/*

# ------------------------------------------------------------------------------
FROM cros-sdk-sources AS cros-sdk-build

# Build SDK and setup the board. Specify the 'dev' USE flag for the libcamera
# package to enable compilation of the development and test tools.
WORKDIR /home/cros/chromiumos
RUN cros_sdk --create --no-ns-pid
RUN cros telemetry --disable
RUN cros_sdk setup_board --board=${cros_board}

USER root
RUN echo 'dev-libs/libevent threads' >> out/build/${cros_board}/etc/portage/package.use
RUN echo 'media-libs/libcamera-upstream dev' >> out/build/${cros_board}/etc/portage/package.use

USER cros
RUN cros workon --board soraka-libcamera start media-libs/libcamera-upstream

# Build the packages needed for libcamera.
RUN cros build-packages \
        --accept-license="*" \
        --board=${cros_board} \
        --jobs=${num_jobs} \
        --no-withautotest \
        --no-withdebugsymbols \
        --no-withdev \
        --no-withfactory \
        --no-withtest \
        media-libs/libcamera-upstream \
        sys-libs/libcxx

# ------------------------------------------------------------------------------
FROM cros-sdk-build AS cros-sdk-toolchain

# Generate a toolchain and a sysroot able to build libcamera.
# cros_setup_toolchains needs to run as root due to b/282231712. The sysroot
# needs sys-libs/libcxx in addition to media-libs/libcamera-upstream as it is
# not listed as an explicit dependency of the libbcamera-upstream package.
RUN cros_sdk sudo cros_setup_toolchains --create-packages \
        --output-dir /build/sdk/ \
        --target x86_64-cros-linux-gnu
RUN cros_sdk cros_generate_sysroot --board ${cros_board} \
        --out-dir /build/sdk/ \
        --package 'media-libs/libcamera-upstream  sys-libs/libcxx'

RUN cros_sdk qtbz2 -s /build/soraka-libcamera/packages/media-libs/libcamera-upstream-9999.tbz2 \
	/build/sdk/libcamera-upstream-9999.tbz2 \
	/build/sdk/libcamera-upstream-9999.xpak

# Unpack the tarballs and copy the xpak to make the SDK available for COPY in
# the final image.
RUN mkdir /home/cros/cros-sdk
WORKDIR /home/cros/cros-sdk
RUN mkdir sysroot toolchain && \
        tar -xf /home/cros/chromiumos/out/build/sdk/sysroot_media-libs_libcamera-upstream.tar.xz -C sysroot/ && \
        tar -xf /home/cros/chromiumos/out/build/sdk/x86_64-cros-linux-gnu.tar.xz -C toolchain/ && \
        rm sysroot_virtual_target-os.tar.xz x86_64-cros-linux-gnu.tar.xz
RUN cp /home/cros/chromiumos/out/build/sdk/libcamera-upstream-9999.xpak .

# ------------------------------------------------------------------------------
FROM docker.io/library/debian:bookworm-slim AS portage-utils
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install --no-install-recommends -y \
        autoconf-archive \
        automake \
        build-essential \
        ca-certificates \
        curl \
        pkg-config

WORKDIR /build
RUN curl -LO https://github.com/gentoo/portage-utils/archive/refs/tags/v0.96.1.tar.gz && \
        tar -xf v0.96.1.tar.gz && \
        rm v0.96.1.tar.gz

WORKDIR portage-utils-0.96.1
RUN ./configure  --disable-qmanifest --disable-qtegrity --prefix=/usr/local && \
        make -j${num_jobs} && \
        make install-exec DESTDIR=../install/
ENV PATH=/build/install/usr/local/bin:$PATH

# ------------------------------------------------------------------------------
FROM docker.io/library/debian:bookworm-slim
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install --no-install-recommends -y \
        ca-certificates \
	file \
        git-core \
	libgomp1 \
        meson \
        ninja-build \
        openssl \
	pax-utils \
        pkg-config \
        python3-jinja2 \
        python3-ply \
        python3-yaml \
	zstd

COPY --from=cros-sdk-toolchain /home/cros/cros-sdk/ /opt/cros-sdk/
COPY meson.x86_64-cros-linux-gnu.amd64.ini /opt/cros-sdk/

COPY --from=portage-utils /build/install/usr/local/bin/* /usr/local/bin/
